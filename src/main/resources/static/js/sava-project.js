$(function () {
    $('.save-project').click(function () {
        var content = $(this).closest('.modal-content').find('table')[0];
        var name = $(this).data("name");
        console.log(content);
        domtoimage.toJpeg(content, { quality: 0.95 })
            .then(function (dataUrl) {
                var link = document.createElement('a');
                link.download = name + '.jpeg';
                link.href = dataUrl;
                link.click();
            });
    });
});