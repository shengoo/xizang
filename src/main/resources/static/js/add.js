var all = {
    name: 'xz',
    cities: [
        {
            name:'日喀则市',
            counties:['仲巴县','康马县','亚东县','吉隆县','岗巴县','桑珠孜区','南木林县','江孜县','萨迦县','拉孜县','白朗县','仁布县','昂仁县','谢通门县','萨嘎县','聂拉木县','定日线县','定结县'],
        },
        {
            name:'昌都市',
            counties:['江达县','类乌齐县','丁青县','察雅县','左贡县','芒康县','洛隆县','边坝县','卡若区','贡觉县','八宿县']
        },
        {
            name:'林芝市',
            counties:['巴宜区','米林县','墨脱县','波密县','工布江达县','察隅县','郎县']
        },
        {
            name:'山南市',
            counties:['措美县','洛扎县','浪卡子县','隆子县','错那县','贡嘎县','乃东县','扎囊县','桑日县','琼结县','曲松县','加查县']
        },
        {
            name:'拉萨市',
            counties:['城关区','林周县','尼木县','曲水县','堆龙德庆县','达孜县','当雄县','墨竹工卡县']
        },
        {
            name:'那曲地区',
            counties:['那曲县','聂荣县','巴青县','安多县','班戈县','比如县索县','嘉黎县','尼玛县','申扎县','双湖县']
        },
        {
            name:'阿里地区',
            counties:['日土县','革吉县','措勤县','噶尔县','普兰县','札达县','改则县']
        }

    ]
};
$(function () {
    // init tag input
    $('#countyArea').tagsinput({
        itemValue: function(item) {
            return item;
        }
    });
    $('.bootstrap-tagsinput input').prop('readonly', true);


    // all the selected
    var selectedCountys = [];
    if($.trim($('#countyArea').val()).length > 0){
        selectedCountys = $('#countyArea').val().split(',');
    }
    var previous = [];

    var cityNames = $.map(all.cities,function (t) {
        return $('<option>' + t.name + '</option>');
    });
    console.log(cityNames)
    $('#city').append(cityNames);
    console.log(cityNames)

    $('#city').change(function () {
        var selectCity = $('#city').val();console.log(selectCity)
        cityChange(selectCity);
    });

    function cityChange(selectCity) {
        var countys = $.grep(all.cities, function(e){return e.name === selectCity; })[0].counties;
        var options = $.map(countys,function (t) {//console.log(t)
            return $('<option>' + t + '</option>');
        });
        $('#county').html(options);
        $('#county').selectpicker('refresh');
        $('#county').selectpicker('val', selectedCountys);
        if(selectedCountys.length > 0)
            $('.filter-option.pull-left').text(selectedCountys);
        if($('#county').val())
            previous = $('#county').val();
        else
            previous = [];
    }

    function getMultiSelectValue() {
        if($('#county').val())
            return $('#county').val();
        else
            return [];
    }

    $('#county').on('changed.bs.select', function (e) {
        var currentVal = getMultiSelectValue();
        console.log('previous:' + previous);
        console.log('current:' + currentVal);
        if(currentVal.length > 0){console.log(previous,currentVal)
            if(previous.length > currentVal.length){
                var item = arrayRemoveArray(previous,currentVal).toString();
                console.log('remove : ' + item);
                $('#countyArea').tagsinput('remove', item);
            }else{
                var item = arrayRemoveArray(currentVal,previous).toString();
                console.log('add : ' + item);
                $('#countyArea').tagsinput('add', item);
            }
        }else {
            $('#countyArea').tagsinput('removeAll');
        }
        //selectedCountys = arrayRemoveArray(selectedCountys,previous).concat(currentVal);
        // previous = currentVal;
        updatePrevious();
        updateSelectedCountysText();
        // $('#countyArea').tagsinput(selectedCountys);
        showfzq();
    });
    $('#countyArea').on('itemRemoved', function(event) {
        // event.item: contains the item
        console.log('itemRemoved : ' + event.item)
        console.log($(this).val())
        $('#county').selectpicker('val', $(this).val().split(','));
        $('#county').selectpicker('refresh');
        updatePrevious();
        updateSelectedCountysText();
    });

    function updatePrevious() {
        // previous = $("#countyArea").val().split(',');
        previous = getMultiSelectValue();
    }

    function updateSelectedCountysText() {
        selectedCountys = $("#countyArea").val().split(',')
        $('.filter-option.pull-left').text(selectedCountys);
    }

    $('form').on('reset',function () {
        $('#county').empty();
        $('#county').selectpicker('val', []);
        $('#county').selectpicker('refresh');
        selectedCountys = [];
        previous = [];
        showfzq();
    });

    $('#city').val(all.cities[0].name);
    cityChange(all.cities[0].name);


    var fzq = [
        {
            name: '自治区水土流失重点预防区',
            countys: [
                '仲巴县',
                '康马县',
                '亚东县',
                '吉隆县',
                '岗巴县',
                '江达县',
                '类乌齐县',
                '丁青县',
                '察雅县',
                '左贡县',
                '芒康县',
                '洛隆县',
                '边坝县',
                '巴宜区',
                '米林县',
                '墨脱县',
                '波密县',
                '工布江达县',
                '察隅县',
                '朗县',
                '措美县',
                '洛扎县',
                '浪卡子县',
                '隆子县',
                '错那县',
                '那曲县',
                '聂荣县',
                '巴青县',
                '安多县',
                '班戈县',
                '当雄县',
                '比如县',
                '索县',
                '嘉黎县',
                '日土县',
                '革吉县',
                '措勤县'
            ]
        },
        {
            name:'自治区水土流失重点治理区',
            countys:[
                '城关区',
                '林周县',
                '尼木县',
                '曲水县',
                '堆龙德庆县',
                '达孜县',
                '墨竹工卡县',
                '贡嘎县',
                '桑珠孜区',
                '南木林县',
                '江孜县',
                '萨迦县',
                '拉孜县',
                '白朗县',
                '仁布县',
                '昂仁县',
                '谢通门县',
                '萨嘎县',
                '卡若区',
                '贡觉县',
                '八宿县',
                '乃东县',
                '扎囊县',
                '桑日县',
                '琼结县',
                '曲松县',
                '加查县',
                '噶尔县',
                '普兰县',
                '札达县'
            ]
        }
    ];
    var fzqVal = [];
    function showfzq() {
        fzqVal = []
        $('#fzq').val('');
        selectedCountys.forEach(function (t) {
            if($.inArray(t,fzq[0].countys)>-1 && $.inArray(fzq[0].name,fzqVal) === -1){
                fzqVal.push(fzq[0].name);
            }
            if($.inArray(t,fzq[1].countys)>-1 && $.inArray(fzq[1].name,fzqVal) === -1){
                fzqVal.push(fzq[1].name);
            }
            $('#fzq').val(fzqVal);
        });
    }

});
function arrayRemoveArray(a1,a2){
    for(var i=0;i<a2.length;i++){
        var index = a1.indexOf(a2[i]);
        if(index > -1){
            console.log('delete' + a2[i])
            a1.splice(index,1);
        }
    }
    return a1;
}