package demo.models;

import demo.annotations.Column;
import demo.services.UserType;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.util.Date;


public class Project implements Serializable {
    @Column
    private long id;
    /**
     * 用户ID
     */
    @Column
    private long userid;

    @Column
    private String username;
    @Column
    private UserType usertype;

    @Column
    private String phone;

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    /**
     * 查询日期
     */
    @Column
    private Date datecreated;

    /**
     * 项目名称
     */
    @Column
    @NotBlank
    private String name;

    /**
     * 项目编号
     */
    @Column
    private String code;

    /**
     * 是否涉密
     */
    @Column
    private boolean isSecure;

    /**
     * 所属行业
     */
    @Column
    private String industry;

    public long getId() {
        return id;
    }



    /**
     * 涉及区县
     */
    @Column
    @NotBlank
    private String county;


    /**
     * 自治区级水土流失重点防治区
     */
    @Column
    private String fzq;

    /**
     * 项目工程征占地面积(公顷)
     */

    @Column
    private double area;

    /**
     * 项目土石方总量(万立方米)
     */
    @Column
    private double sum;

    
    /**
     * 水土保持分类管理登记
     */
    @Column
    private String dengji;
    
    /**
     * 水土保持方案形式
     */
    @Column
    private String xingshi;
    
    
    /**
     * 水土保持方案审批
     */
    @Column
    private String shenpi;
    
    
    /**
     * 监督检查
     */
    @Column
    private String jiandujiancha;
    
    
    /**
     * 水土保持后续设计
     */
    @Column
    private String houxusheji;
    
    
    /**
     * 水土保持监理
     */
    @Column
    private String jianli;
    
    /**
     * 水土保持监测
     */
    @Column
    private String jiance;
    
    /**
     * 水土保持设施验收
     */
    @Column
    private String sheshiyanshou;
    

    public void setId(long id) {
        this.id = id;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSecure() {
        return isSecure;
    }

    public void setSecure(boolean secure) {
        isSecure = secure;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }



    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getFzq() {
        return fzq;
    }

    public void setFzq(String fzq) {
        this.fzq = fzq;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

	public String getDengji() {
		return dengji;
	}

	public void setDengji(String dengji) {
		this.dengji = dengji;
	}

	public String getXingshi() {
		return xingshi;
	}

	public void setXingshi(String xingshi) {
		this.xingshi = xingshi;
	}

	public String getShenpi() {
		return shenpi;
	}

	public void setShenpi(String shenpi) {
		this.shenpi = shenpi;
	}

	public String getJiandujiancha() {
		return jiandujiancha;
	}

	public void setJiandujiancha(String jiandujiancha) {
		this.jiandujiancha = jiandujiancha;
	}

	public String getHouxusheji() {
		return houxusheji;
	}

	public void setHouxusheji(String houxusheji) {
		this.houxusheji = houxusheji;
	}

	public String getJianli() {
		return jianli;
	}

	public void setJianli(String jianli) {
		this.jianli = jianli;
	}

	public String getJiance() {
		return jiance;
	}

	public void setJiance(String jiance) {
		this.jiance = jiance;
	}

	public String getSheshiyanshou() {
		return sheshiyanshou;
	}

	public void setSheshiyanshou(String sheshiyanshou) {
		this.sheshiyanshou = sheshiyanshou;
	}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserType getUsertype() {
        return usertype;
    }

    public void setUsertype(UserType usertype) {
        this.usertype = usertype;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", userid=" + userid +
                ", username='" + username + '\'' +
                ", usertype=" + usertype +
                ", phone='" + phone + '\'' +
                ", datecreated=" + datecreated +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", isSecure=" + isSecure +
                ", industry='" + industry + '\'' +
                ", county='" + county + '\'' +
                ", fzq='" + fzq + '\'' +
                ", area=" + area +
                ", sum=" + sum +
                ", dengji='" + dengji + '\'' +
                ", xingshi='" + xingshi + '\'' +
                ", shenpi='" + shenpi + '\'' +
                ", jiandujiancha='" + jiandujiancha + '\'' +
                ", houxusheji='" + houxusheji + '\'' +
                ", jianli='" + jianli + '\'' +
                ", jiance='" + jiance + '\'' +
                ", sheshiyanshou='" + sheshiyanshou + '\'' +
                '}';
    }
}
