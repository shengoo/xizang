package demo.models;

/**
 * Created by yelei on 2017/11/8.
 */
public class Result<T> {

    private long code;
    private String message;
    private T result;

    public Result(long code, String message, T result) {
        super();
        this.code = code;
        this.message = message;
        this.result = result;
    }

    public Result(long code, T result) {
        super();
        this.code = code;
        this.message = "";
        this.result = result;
    }

    public Result(long code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public Result(long code) {
        super();
        this.code = code;
        this.message = "";
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

}