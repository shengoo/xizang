package demo.models;

/**
 * Created by yelei on 2017/11/8.
 */
public interface Code {
    long SUCCESS = 0;
    long FAILED = -1;
    long PHONE_NULL = 10000 ;//手机号号码不能为空
    long VERIFY_LEGAL = 10001; //手机号码格式不正确
    long VERIFY_ERROR = 10002; //验证码发送错误!
    long USER_LOGIN_NULL =10003 ;// 登录名或密码不能为空
    long PASS_ERROR =10004 ;// 密码错误
    long USER_ENABLED =10006 ;// 用户被禁用
    long VERIFY_CODE_ERROR = 10007 ; //验证码错误
    long PASS_DIF = 10008 ; //两次密码不同
    long USRENAME_Registered =10009 ; //用户名已注册
    long PHONE_Registered =10010 ; //手机号已注册

    long USER_NOT_Register =10011 ;//用户名未注册
    long PHONE_DIF = 10012; //手机号不符


    long REGISTER_ERROR = 10003; //注册用户异常!
    long TOKEN_NOT_EXIST= 10004; //登录过期!
    long TOKEN_NULL= 10005; //登录token不能为空
    long FILE_OUT_LIMIT = 10006; //文件过大
    long FILE_NOT_SUPPORTED = 10007; //文件格式不支持
    long USER_TARGET_NULL = 10008; //userId和targetId不能为空
    long CONTENT_STAR_NULL = 10009; //评论内容不能为空!
    long TARGET_TYPE_NULL = 10010; //类型不能为空!
    long SUGGEST_CONTENT= 10011; //建议内容不能为空!
    long LONG_LATI_NULL = 10012; //经纬度不能为空!
    long TOKEN_USERID_NULL = 10013;//登陆过期!
    long TYPE_TAG_NULL = 10014; //地址类型不能为空!
    long TAG_ID_NULL = 10015; //项目Id不能为空!
    long SCHEDULE_NULL = 10016; //预约详情不能为空!
    long ORDER_NULL = 10017; //预约信息不全!
    long USER_TYPE_NULL = 10018; //用户Id和type不能为空!
    long USER_SCHEDULE_USED = 10019; //发布详情不能为空!
    long TAG_ID_EXIST = 10020; //项目已经存在!
    long USER_NOT_EXIST = 10021; //用户已经存在!
    long TOKEN_ERROR = 10022;//登录异常!
    long USER_BAN=10023;//用户已禁用
    long PUSH_PARAM_ERROR=10024;//推送参数异常!
    long USER_ID_NULL = 10025; //用户Id不能为空!
    long USER_PROFILE_NULL = 10026;//用户详情不能为空!
    long ORDRE_TYPE_NULL = 10027;//预约类型不能为空!
    long TAG_TYPE_NULL = 10028;//项目类型不能为空!
    long TARGETID_EQU_USERID = 10029;//
    long SEACHER_RESULT_TEMP = 10030;// 查询接口为空
    long PAY_PARAM_NULL = 30001 ;// 支付参数异常 !
    long AuthenticationException = 30002;//认证异常
    long InvalidRequestException = 30003;//无效请求
    long APIConnectionException = 30004; //API链接异常
    long APIException = 30005;   //API异常
    long ChannelException = 30006;  //支付渠道一次
}
