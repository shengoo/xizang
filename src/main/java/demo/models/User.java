package demo.models;


import demo.services.UserType;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    private int id;

    private String username;
    private String password;
    private String phone;
    private String email;

    private boolean enabled;

    private Date lastvisittime;

    public boolean isAdmin(){
        return this.userType.equals(UserType.Admin);
    }

    public UserType getUserType() {
        return userType;
    }


    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    private UserType userType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getLastvisittime() {
        return lastvisittime;
    }

    public void setLastvisittime(Date lastvisittime) {
        this.lastvisittime = lastvisittime;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", enabled=" + enabled +
                ", userType=" + userType +
                '}';
    }
}
