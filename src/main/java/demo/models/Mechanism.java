package demo.models;

import demo.annotations.Column;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

public class Mechanism {
    @Column
    int id;
    @Column
    @NotBlank
    String abbr;
    @Column
    String leader;
    @Column
    String leadercontact;
    @Column
    String contact;
    @Column
    String contactcontact;
    @Column
    String address;
    @Column
    String supervisor;
    @Column
    Date datecreated;
    @Column
    Date dateupdated;

    @Column
    String email;

    public String getEmail() {
        if(email == null || email.isEmpty())
            return "--";
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Mechanism{" +
                "id=" + id +
                ", abbr='" + abbr + '\'' +
                ", leader='" + leader + '\'' +
                ", leadercontact='" + leadercontact + '\'' +
                ", contact='" + contact + '\'' +
                ", contactcontact='" + contactcontact + '\'' +
                ", address='" + address + '\'' +
                ", supervisor='" + supervisor + '\'' +
                ", datecreated=" + datecreated +
                ", dateupdated=" + dateupdated +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getLeadercontact() {
        return leadercontact;
    }

    public void setLeadercontact(String leadercontact) {
        this.leadercontact = leadercontact;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactcontact() {
        return contactcontact;
    }

    public void setContactcontact(String contactcontact) {
        this.contactcontact = contactcontact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    public Date getDateupdated() {
        return dateupdated;
    }

    public void setDateupdated(Date dateupdated) {
        this.dateupdated = dateupdated;
    }
}
