package demo.utils;

import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by yelei on 2017/11/8.
 */
public class StringUtil {

    public static String randomNum(int randomLength) {
        char[] randoms = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        char[] randoms1 = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        Random random = new Random();
        StringBuffer ret = new StringBuffer();
        ret.append(randoms1[random.nextInt(randoms1.length)]);
        for (int i = 1; i < randomLength; i++) {
            ret.append(randoms[random.nextInt(randoms.length)]);
        }
        random = null;
        return ret.toString();
    }

}
