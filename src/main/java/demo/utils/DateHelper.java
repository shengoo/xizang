package demo.utils;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {
    public static Date getTomorrow(Date from){
        Calendar c = Calendar.getInstance();
        c.setTime(from);
        c.add(Calendar.DATE, 1);
        Date tomorrow = c.getTime();
        return tomorrow;
    }
}
