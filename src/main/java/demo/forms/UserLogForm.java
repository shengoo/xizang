package demo.forms;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class UserLogForm implements Serializable {

    private String username;
    private String projectname;

    private String level;
    private int pageno;

    @DateTimeFormat(pattern="yyyy-M-d")
    private Date dateFrom;
    @DateTimeFormat(pattern="yyyy-M-d")
    private Date dateTo;

    public UserLogForm() {
        this.username = "";
        this.projectname = "";
        this.level = "A类";
        this.pageno = 1;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public int getPageno() {
        return pageno;
    }

    public void setPageno(int pageno) {
        this.pageno = pageno;
    }

    @Override
    public String toString() {
        return "UserLogForm{" +
                "username='" + username + '\'' +
                ", projectname='" + projectname + '\'' +
                ", level='" + level + '\'' +
                ", pageno=" + pageno +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                '}';
    }
}
