package demo.forms;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class RegisterForm implements Serializable {

	@NotBlank
	private String username;

	@NotBlank
	@Size(max=11,min=11,message="请输入正确的电话号码")
	private String phone;

	@NotBlank
	@Size(min=6,max=6,message="请输入正确的验证码")
	private String code;

	@NotBlank
	@Pattern(regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,30}$", message = "6-30位字母或数字组合，区分大小写")
	private String password;

//	@NotBlank
//	@Size(min = 1)
//	private String repeatPassword;

	@Email
	private String email;


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

//	public String getRepeatPassword() {
//		return repeatPassword;
//	}
//
//	public void setRepeatPassword(String repeatPassword) {
//		this.repeatPassword = repeatPassword;
//	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
}
