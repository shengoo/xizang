package demo.forms;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

public class UserSearchForm implements Serializable {

    private String q;
    private int pageno;

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q.trim().isEmpty() ? q.trim() : q;
    }

    public int getPageno() {
        return pageno;
    }

    public void setPageno(int pageno) {
        this.pageno = pageno;
    }

    @Override
    public String toString() {
        return "UserSearchForm{" +
                "q='" + q + '\'' +
                ", pageno=" + pageno +
                '}';
    }
}
