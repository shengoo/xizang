package demo.forms;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ProjectSearchForm implements Serializable {
    private String county;
    private String industry;
    private String dengji;
    private String areaFrom;
    private String areaTo;
    @DateTimeFormat(pattern="yyyy-M-d")
    private Date dateFrom;
    @DateTimeFormat(pattern="yyyy-M-d")
    private Date dateTo;
    
    private String sumFrom;
    private String sumTo;

    private int pageno;

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getDengji() {
        return dengji;
    }

    public void setDengji(String dengji) {
        this.dengji = dengji;
    }

    public String getAreaFrom() {
        return areaFrom;
    }

    public void setAreaFrom(String areaFrom) {
        this.areaFrom = areaFrom;
    }

    public String getAreaTo() {
        return areaTo;
    }

    public void setAreaTo(String areaTo) {
        this.areaTo = areaTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }
    
    public void setDateFrom(Date dateFrom) {
    		this.dateFrom = dateFrom;
    }

//    public void setDateFrom(String dateFrom) {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-d");
//        try {
//            this.dateFrom = formatter.parse(dateFrom);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
    		this.dateTo = dateTo;
    }
    
//    public void setDateTo(String dateTo) {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-d");
//        try {
//            this.dateTo = formatter.parse(dateTo);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

    public String getSumFrom() {
        return sumFrom;
    }

    public void setSumFrom(String sumFrom) {
        this.sumFrom = sumFrom;
    }

    public String getSumTo() {
        return sumTo;
    }

    public void setSumTo(String sumTo) {
        this.sumTo = sumTo;
    }

    public int getPageno() {
        return pageno;
    }

    public void setPageno(int pageno) {
        this.pageno = pageno;
    }

    @Override
    public String toString() {
        return "ProjectSearchForm{" +
                "county='" + county + '\'' +
                ", industry='" + industry + '\'' +
                ", dengji='" + dengji + '\'' +
                ", areaFrom=" + areaFrom +
                ", areaTo=" + areaTo +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", sumFrom=" + sumFrom +
                ", sumTo=" + sumTo +
                '}';
    }
}
