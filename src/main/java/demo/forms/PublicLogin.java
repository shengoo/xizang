package demo.forms;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

public class PublicLogin implements Serializable {


	@NotBlank
	@Size(max=11,min=11,message="请输入正确的电话号码")
	private String phone;
	
	@NotBlank
	@Size(min=6,max=6,message="请输入正确的验证码")
	private String code;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
