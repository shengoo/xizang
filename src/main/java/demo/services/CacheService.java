package demo.services;

import demo.models.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by yelei on 2017/12/10.
 */
@Service
public class CacheService {
    private static Map<String, String> data = new HashMap<>();//用与缓存模拟数据

    private static Map<String, User> userdata = new HashMap<String, User>();//用与缓存模拟数据
    /**
     * 缓存的key
     */
    public static final String KEY = "Phone";
    /**
     * value属性表示使用哪个缓存策略，缓存策略在ehcache.xml
     */
    public static final String CACHE_VALUES = "verifyCodeCache";

    public static final String CACHE_VALUES1 = "userCache";

    @CacheEvict(value = CACHE_VALUES, key = KEY)
    public void putCache(String phone,String code) {
        data.put(phone, code);
    }

    @CacheEvict(value = CACHE_VALUES)
    public void removeCache(String phoned) {
        data.remove(phoned);
    }

    @Cacheable(value = CACHE_VALUES)
    public String findCacheByPhone(String phone) {
        return data.get(phone);
    }

    @CacheEvict(value = CACHE_VALUES1, key = KEY)
    public void putUserCache(String phone,User user) {
        userdata.put(phone, user);
    }

    @Cacheable(value = CACHE_VALUES1, key = KEY)
    public User findUserByPhone(String phone) {
        return userdata.get(phone);
    }

    @CacheEvict(value = CACHE_VALUES1)
    public void removeUserCache(String phoned) {
        userdata.remove(phoned);
    }

}
