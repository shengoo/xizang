package demo.services;

public enum UserType {
	Guest,
	User,
	Admin
}
