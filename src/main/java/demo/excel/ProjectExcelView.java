package demo.excel;

import demo.models.Project;
import demo.models.User;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class ProjectExcelView extends AbstractXlsView {
    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-M-d");
//    String date = DATE_FORMAT.format(today);


    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String excelName = model.get("name").toString() + ".xls";
        response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(excelName,"utf-8"));
        response.setContentType("application/ms-excel; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        List<Project> list = (List<Project>) model.get("members");
        Sheet sheet = workbook.createSheet("用户信息");
        sheet.setDefaultColumnWidth(30);
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
        style.setFont(font);
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("项目名称");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("项目编号");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("所属行业");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("涉及区县");
        header.getCell(3).setCellStyle(style);
        header.createCell(4).setCellValue("自治区级水土流失重点防治区");
        header.getCell(4).setCellStyle(style);
        header.createCell(5).setCellValue("项目占地面积（公顷）");
        header.getCell(5).setCellStyle(style);
        header.createCell(6).setCellValue("项目土石方总量（万立方米）");
        header.getCell(6).setCellStyle(style);
        header.createCell(7).setCellValue("水土保持分类");
        header.getCell(7).setCellStyle(style);
        header.createCell(8).setCellValue("水土保持方案形式");
        header.getCell(8).setCellStyle(style);
        header.createCell(9).setCellValue("查询日期");
        header.getCell(9).setCellStyle(style);
        header.createCell(10).setCellValue("用户ID");
        header.getCell(10).setCellStyle(style);
        int rowCount = 1;
        for (Project item : list) {
            Row userRow = sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(item.getName());
            userRow.createCell(1).setCellValue(item.getCode());
            userRow.createCell(2).setCellValue(item.getIndustry());
            userRow.createCell(3).setCellValue(item.getCounty());
            userRow.createCell(4).setCellValue(item.getFzq());
            userRow.createCell(5).setCellValue(item.getArea());
            userRow.createCell(6).setCellValue(item.getSum());
            userRow.createCell(7).setCellValue(item.getDengji());
            userRow.createCell(8).setCellValue(item.getXingshi());
            if (item.getDatecreated() != null) userRow.createCell(9).setCellValue(DATE_FORMAT.format(item.getDatecreated()));
            else userRow.createCell(9).setCellValue("--");
            userRow.createCell(10).setCellValue(item.getUserid());
        }
    }
}
