package demo.excel;

import demo.models.User;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class UserExcelView extends AbstractXlsView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String excelName = model.get("name").toString() + ".xls";
        response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(excelName,"utf-8"));
        response.setContentType("application/ms-excel; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        List<User> list = (List<User>) model.get("members");
        Sheet sheet = workbook.createSheet("用户信息");
        sheet.setDefaultColumnWidth(30);
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("用户名");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("手机号");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("登录密码");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("联系邮箱");
        header.getCell(3).setCellStyle(style);
        header.createCell(4).setCellValue("状态");
        header.getCell(4).setCellStyle(style);
        int rowCount = 1;
        for (User user : list) {
            Row userRow = sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(user.getUsername());
            userRow.createCell(1).setCellValue(user.getPhone());
            userRow.createCell(2).setCellValue(user.getPassword());
            userRow.createCell(3).setCellValue(user.getEmail());
            userRow.createCell(4).setCellValue(user.isEnabled()?"可用":"已注销");
        }
    }
}
