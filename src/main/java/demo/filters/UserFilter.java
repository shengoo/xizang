package demo.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import demo.models.User;
import demo.services.UserType;
import org.slf4j.LoggerFactory;

@WebFilter(filterName = "UserFilter", urlPatterns = "/project*")
public class UserFilter implements Filter {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UserFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
//        logger.info("UserFilter doFilter");
		HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String url = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());
//        logger.info(url);
//        if (url.startsWith("/") && url.length() > 1) {
//            url = url.substring(1);
//        }
        HttpSession session = httpRequest.getSession();
        User user = (User) session.getAttribute("USER");

        if((url.startsWith("/project") || url.startsWith("/admin") || url.startsWith("/export")) && user == null){
            httpResponse.sendRedirect("/");
            return;
        }

        if(url.startsWith("/admin") && user != null && user.getUserType() != UserType.Admin){
            httpResponse.sendRedirect("/project/add");
            return;
        }

        chain.doFilter(httpRequest, httpResponse);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
