package demo.api.controller;

//import com.sun.org.glassfish.external.statistics.annotations.Reset;
import demo.forms.ForgetForm;
import demo.forms.RegisterForm;
import demo.models.UserLogin;
import demo.mappers.UserMapper;
import demo.models.Code;
import demo.models.Result;
import demo.models.User;
import demo.services.CacheService;
import demo.services.SmsService;
import demo.services.SmsType;
import demo.services.UserType;
import demo.utils.AccountValidatorUtil;
import demo.utils.PasswordHelper;
import demo.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;

/**
 * Created by yelei on 2017/11/8.
 */
@RestController
@RequestMapping("/api/account")
public class ApiAccountController {

    @Autowired
    private SmsService smsService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CacheService cacheService;


    @RequestMapping( "userlogin" )
    public Result<User> userlogin(@RequestBody UserLogin userLogin, HttpServletRequest request){

        //用户登录
        if ("1".equals(userLogin.getType())){
            //判断是否为空
            if(userLogin.getUsername() ==null || "".equals(userLogin.getUsername()) ||userLogin.getPassword() ==null || "".equals(userLogin.getPassword())){
                return new Result<User>(Code.USER_LOGIN_NULL,"登录名或密码不能为空!", null);
            }else {
                User user = userMapper.validateUser(userLogin.getUsername(), PasswordHelper.encryptPassword(userLogin.getPassword()));
                if(user == null){
                    return new Result<User>(Code.PASS_ERROR,"密码错误!", null);
                }
                if (!user.isEnabled()){
                    return new Result<User>(Code.USER_ENABLED,"用户被禁用!", null);
                }
//               HttpSession session = request.getSession();
//               session.setAttribute("USER",user);
                cacheService.removeUserCache(user.getUsername());
                cacheService.putUserCache(user.getUsername(),user);
                return new Result<User>(Code.SUCCESS,"用户登录成功!", user);
            }
        }else {
            //游客登录
            if(userLogin.getUsername() ==null || "".equals(userLogin.getUsername()) ||userLogin.getPassword() ==null || "".equals(userLogin.getPassword())){
                return new Result<User>(Code.USER_LOGIN_NULL,"手机号码或验证码不能为空!", null);
            }else {
                // HttpSession session = request.getSession();
//               String phone = (String) session.getAttribute("VALIDATE_PHONE");
//               String code = (String) session.getAttribute("VALIDATE_PHONE_CODE");
                String code = cacheService.findCacheByPhone(userLogin.getUsername());
                if ( code == null || "".equals(code))
                    return new Result<User>(Code.FAILED,"游客登录失败!", null);
                if( code.equals(userLogin.getPassword())){
                    User user = userMapper.getGuest(userLogin.getUsername());
                    if(user == null){
                        user = new User();
                        user.setUsername(userLogin.getUsername());
                        user.setPhone(userLogin.getUsername());
                        user.setUserType(UserType.Guest);
                        userMapper.addOne(user);
                        user = userMapper.getGuest(userLogin.getUsername());
                    }
                    cacheService.removeUserCache(user.getUsername());
                    cacheService.putUserCache(user.getUsername(),user);
                    return new Result<User>(Code.SUCCESS,"游客登录成功!", user);
                }else{
                    return new Result<User>(Code.PASS_ERROR,"请输入正确的验证码!", null);
                }
            }
        }
    }

    @RequestMapping(value = "register")
    public Result<String> register (@RequestBody RegisterForm form, HttpServletRequest request){
        String code =  cacheService.findCacheByPhone(form.getPhone());
        if (code == null || "".equals(code))
            return new Result<String>(Code.FAILED,"注册失败!", null);

        if (!code.equals(form.getCode())){
            return new Result<String>(Code.VERIFY_CODE_ERROR,"请输入正确的验证码!", "");
        }

        User existing = userMapper.getByName(form.getUsername());
        if(existing != null) {
            return new Result<String>(Code.USRENAME_Registered,"用户名已注册!", "");
        }
        User checkPhone = userMapper.getByPhone(form.getPhone());
        if(checkPhone != null) {
            return new Result<String>(Code.PHONE_Registered,"手机号已注册!", "");
        }
        User user = new User();
        user.setPhone(form.getPhone());
        user.setUserType(UserType.User);
        user.setUsername(form.getUsername());
        user.setPassword(PasswordHelper.encryptPassword(form.getPassword()));
        userMapper.addOne(user);
        return new Result<String>(Code.SUCCESS,"注册成功!", "");
    }


    @RequestMapping("forget")
    public Result<String> forget(@RequestBody ForgetForm form, HttpServletRequest request){
        String code =  cacheService.findCacheByPhone(form.getPhone());
        if ( code == null || "".equals(code))
            return new Result<String>(Code.FAILED,"注册失败!", null);

        if ( !code.equals(form.getCode())){
            return new Result<String>(Code.VERIFY_CODE_ERROR,"请输入正确的验证码!", "");
        }

        User user = userMapper.getByName(form.getUsername());
        if(user == null){
            return new Result<String>(Code.USER_NOT_Register,"用户名未注册!", "");
        }

        if(!user.getPhone().equals(form.getPhone())){
            return new Result<String>(Code.PHONE_DIF,"手机号不符!", "");
        }
        userMapper.updatePassword(user.getId(), PasswordHelper.encryptPassword(form.getPassword()));
        return new Result<String>(Code.SUCCESS,"密码修改成功!", "");
    }


    @ResponseBody
    @GetMapping(value = "verify_code")
    public Result<String> verifyCode(@RequestParam("phone") String phone,@RequestParam("type") String type, HttpServletRequest request){
        //1 注册 ；2 登录  ； 3 找回
        SmsType oper = SmsType.Login;
        if ("1".equals(type))
            oper =  SmsType.Register;
        else if ("2".equals(type))
            oper =  SmsType.Login;
        else if ("3".equals(type))
            oper =  SmsType.Forget;
        else
            oper =  SmsType.Login;

        return SendCode(phone,request,oper);
    }


    /**
     *  发送短信
     * @param phone
     * @param request
     * @return
     */
    private Result<String> SendCode(String phone,HttpServletRequest request,SmsType type){
        //判断是否为空
        if(phone ==null || "".equals(phone)){
            return new Result<String>(Code.PHONE_NULL,"手机号码不能为空!", "");
        }else{
            //判断电话号码规则是否正确
            if(AccountValidatorUtil.isMobile(phone)){
                String verifyCode = StringUtil.randomNum(6);
//                HttpSession session = request.getSession();
//                session.setAttribute("VALIDATE_PHONE", phone);
//                session.setAttribute("VALIDATE_PHONE_CODE", verifyCode);
//                session.setAttribute("SEND_CODE_TIME", new Date().getTime());
                //  SmsType smsType = SmsType.valueOf(type);
                boolean ret = smsService.sendSms(type, phone, verifyCode);
                if (ret){
                    cacheService.removeCache(phone);
                    cacheService.putCache(phone, verifyCode);
                    return new Result<String>(Code.SUCCESS,"验证码发送成功!", verifyCode);
                }else{
                    return new Result<String>(Code.VERIFY_ERROR,"验证码发送错误!", "");
                }
            }else{
                return new Result<String>(Code.VERIFY_LEGAL,"手机号码格式不正确!", "");
            }
        }
    }

}
