package demo.api.controller;

import demo.forms.ProjectSearchForm2;
import demo.mappers.ProjectMapper;
import demo.mappers.UserMapper;
import demo.models.Code;
import demo.models.Project;
import demo.models.Result;
import demo.models.User;
import demo.services.CacheService;
import demo.services.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by yelei on 2017/11/12.
 */
@RestController
@RequestMapping("/api/project")
public class ApiProjectController {

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private UserMapper userMapper;

    private boolean chageCountryType(String country){
        boolean b = false;
        if (country.indexOf("改则县") !=-1){
            b = true;
        }
        if (country.indexOf("尼玛县") !=-1){
            b = true;
        }
        if (country.indexOf("申扎县") !=-1){
            b = true;
        }
        if (country.indexOf("双湖县") !=-1){
            b = true;
        }
        if (country.indexOf("聂拉木县") !=-1){
            b = true;
        }
        if (country.indexOf("定日线县")!=-1){
            b = true;
        }
        if (country.indexOf("定结县。") !=-1){
            b = true;
        }
        return  b;
    }

    @RequestMapping("submit")
    public Result<String> submitProject(@RequestBody Project form, HttpServletRequest request){
        if (form.getArea() >= 100 && form.getSum() >= 100 && !chageCountryType(form.getCounty())) {
            form.setDengji("A类");
            form.setXingshi("报告书（A版）");
            form.setShenpi("评审机构先组织查看现场，再组织评审，评审结果报自治区水行政主管部门批准。");
            form.setJiandujiancha("组织实地监督检查。");
            form.setHouxusheji("主体工程初步设计中应有水土保持篇章；施工图设计阶段应进行专项水土保持设计。");
            form.setJianli("1、开展水土保持专项监理 \n" +
                    "2、验收时提交水土保持监理报告");
            form.setJiance("1、开展水土保持监测 \n" +
                    "2、分别向区、地、县提交监测实施方案、季报、年报 \n" +
                    "3、竣工验收阶段提交监测总结报告");
            form.setSheshiyanshou("投产使用前组织第三方机构编制水土保持设施验收报告，开展水土保持设施专项验收，向社会公告验收情况，验收成果向原审批机关报备。");

            form.setDatecreated(new Date());
            // HttpSession session = request.getSession();

            if(cacheService.findUserByPhone(form.getPhone()) != null) {
                User user = cacheService.findUserByPhone(form.getPhone());
                form.setUserid(user.getId());
                form.setUsername(user.getUsername());
                form.setPhone(user.getPhone());
            }

            projectMapper.insertProject(form);
            System.out.println(form.toString());
            return new Result<String>(Code.SUCCESS,"提交成功!", "");
        }
        if(form.getArea() >= 100 || form.getSum() >= 100){
            form.setDengji("A类");
            form.setXingshi("报告书（A版）");
            form.setShenpi("评审机构组织评审，评审结果报自治区水行政主管部门批准。");
            form.setJiandujiancha("组织实地检查、会议检查、书面检查等方式进行监督检查。");
            form.setHouxusheji("主体工程初步设计中应有水土保持篇章；施工图设计阶段应进行专项水土保持设计。");
            form.setJianli("1、开展水土保持专项监理 \n" +
                    "2、验收时提交水土保持监理报告");
            form.setJiance("1、开展水土保持监测 \n" +
                    "2、分别向区、地、县提交监测实施方案、季报、年报 \n" +
                    "3、竣工验收阶段提交监测总结报告");
            form.setSheshiyanshou("投产使用前组织第三方机构编制水土保持设施验收报告，开展水土保持设施专项验收，向社会公告验收情况，验收成果向原审批机关报备。");

            form.setDatecreated(new Date());
            if(cacheService.findUserByPhone(form.getPhone()) != null) {
                User user = cacheService.findUserByPhone(form.getPhone());
                form.setUserid(user.getId());
                form.setUsername(user.getUsername());
                form.setPhone(user.getPhone());
            }
            projectMapper.insertProject(form);
            System.out.println(form.toString());
            return new Result<String>(Code.SUCCESS,"提交成功!", "");
        }

        if((form.getArea() >= 50 && form.getArea() < 100) || (form.getSum()  >= 50 && form.getSum() < 100)){
            form.setDengji("B类");
            form.setXingshi("报告书（B版）");
            form.setShenpi("评审机构组织评审，评审结果报自治区水行政主管部门批准。");
            form.setJiandujiancha("组织实地检查、会议检查、书面检查等方式进行监督检查。");
            form.setHouxusheji("主体工程初步设计中应有水土保持篇章");
            form.setJianli("1、主体工程监理单位中应有经注册的水土保持监理工程师；\n" +
                    "2、验收时提交水土保持监理报告。");
            form.setJiance("1、开展水土保持监测 \n" +
                    "2、分别向区、地、县提交监测实施方案、季报、年报 \n" +
                    "3、竣工验收阶段提交监测总结报告");
            form.setSheshiyanshou("投产使用前组织第三方机构编制水土保持设施验收报告，开展水土保持设施专项验收，向社会公告验收情况，验收成果向原审批机关报备。");

            form.setDatecreated(new Date());
            if(cacheService.findUserByPhone(form.getPhone()) != null) {
                User user = cacheService.findUserByPhone(form.getPhone());
                form.setUserid(user.getId());
                form.setUsername(user.getUsername());
                form.setPhone(user.getPhone());
            }
            projectMapper.insertProject(form);
            System.out.println(form.toString());
            return new Result<String>(Code.SUCCESS,"提交成功!", "");
        }

        if((form.getArea() >= 20 && form.getArea() < 50) || (form.getSum()  >= 20 && form.getSum() < 50)){
            form.setDengji("B类");
            form.setXingshi("报告书（B版）");
            form.setShenpi("评审机构组织评审，评审结果报自治区水行政主管部门批准。");
            form.setJiandujiancha("组织实地检查、会议检查、书面检查等方式进行监督检查。");
            form.setHouxusheji("主体工程初步设计中应有水土保持篇章");
            form.setJianli("1、主体工程监理单位中应有经注册的水土保持监理工程师；\n" +
                    "2、验收时提交水土保持监理报告。");
            form.setJiance("1、开展水土保持监测 \n" +
                    "2、分别向区、地、县提交监测实施方案、季报、年报 \n" +
                    "3、竣工验收阶段提交监测总结报告");
            form.setSheshiyanshou("投产使用前组织第三方机构编制水土保持设施验收报告，开展水土保持设施专项验收，向社会公告验收情况，验收成果向原审批机关报备。");

            form.setDatecreated(new Date());
            if(cacheService.findUserByPhone(form.getPhone()) != null) {
                User user = cacheService.findUserByPhone(form.getPhone());
                form.setUserid(user.getId());
                form.setUsername(user.getUsername());
                form.setPhone(user.getPhone());
            }
            projectMapper.insertProject(form);
            System.out.println(form.toString());
            return new Result<String>(Code.SUCCESS,"提交成功!", "");
        }

        if((form.getArea() >= 1 && form.getArea() < 20) || (form.getSum()  >= 1 && form.getSum() < 20)){
            form.setDengji("C类");
            form.setXingshi("报告书（C版）");
            form.setShenpi("地（市）水行政主管部门审批，跨地（市）的项目由自治区水行政主管部门审批。");
            form.setJiandujiancha("地（市）、县（区）水行政主管部门按属地原则，组织实施水土保持监督检查。");
            form.setHouxusheji("主体工程初步设计中应有水土保持内容");
            form.setJianli("主体工程监理中应有水土保持监理相关内容");
            form.setJiance("鼓励开展水土保持监测");
            form.setSheshiyanshou("与主体工程一并验收，验收成果由验收主持单位和建设单位自行存档备查。");
            form.setDatecreated(new Date());
            if(cacheService.findUserByPhone(form.getPhone()) != null) {
                User user = cacheService.findUserByPhone(form.getPhone());
                form.setUserid(user.getId());
                form.setUsername(user.getUsername());
                form.setPhone(user.getPhone());
            }
            projectMapper.insertProject(form);
            System.out.println(form.toString());
            return new Result<String>(Code.SUCCESS,"提交成功!", "");
        }
        if(form.getArea() < 1 || form.getSum() < 1){
            form.setDengji("C类");
            form.setXingshi("报告表");
            form.setShenpi("水土保持方案审批权限下放到地（市）水行政主管部门，跨地市的项目除外。");
            form.setJiandujiancha("地（市）、县水行政主管部门按属地原则，组织实施水土保持监督检查。");
            form.setHouxusheji("主体工程初步设计中应有水土保持内容");
            form.setJianli("主体工程监理中应有水土保持监理相关内容");
            form.setJiance("鼓励开展水土保持监测");
            form.setSheshiyanshou("与主体工程一并验收，验收成果由验收主持单位和建设单位自行存档备查。");
            form.setDatecreated(new Date());
            if(cacheService.findUserByPhone(form.getPhone()) != null) {
                User user = cacheService.findUserByPhone(form.getPhone());
                form.setUserid(user.getId());
                form.setUsername(user.getUsername());
                form.setPhone(user.getPhone());
            }
            projectMapper.insertProject(form);
            System.out.println(form.toString());
            return new Result<String>(Code.SUCCESS,"提交成功!", "");
        }else{
            return new Result<String>(Code.SUCCESS,"提交成功!", "");
        }
    }


//    @RequestMapping("search")
//    public Result<java.util.List<Project>> searchProject(@RequestBody ProjectSearchForm2 form , HttpServletRequest request){
//
//        int pageSize = 10;
//
//        java.util.List<Project> all = projectMapper.getAllByUserId(form.getUserid());
//
//        if(form.getAreaFrom()!=null && !form.getAreaFrom().isEmpty()){
//            float areaFrom = Float.parseFloat(form.getAreaFrom());
//            all.removeIf(project -> project.getArea() < areaFrom);
//        }
//        if(form.getAreaTo()!=null && !form.getAreaTo().isEmpty()){
//            float areaTo = Float.parseFloat(form.getAreaTo());
//            all.removeIf(project -> project.getArea() >= areaTo);
//        }
//        if(form.getDateFrom()!=null){
//            all.removeIf(project -> project.getDatecreated().before(form.getDateFrom()));
//        }
//        if(form.getDateTo()!=null){
//            all.removeIf(project -> project.getDatecreated().after(form.getDateTo()));
//        }
//        if(!(form.getCounty() == null) && !form.getCounty().isEmpty() && !form.getCounty().equals("null")){
//            String[] countys = form.getCounty().split(",");
//            all.removeIf(project-> !Arrays.stream(countys).parallel().anyMatch(project.getCounty()::contains));
//        }
//        if(!(form.getDengji() == null) && !form.getDengji().isEmpty() && !form.getDengji().equals("null")){
//            java.util.List<String> strings = Arrays.asList(form.getDengji().split(","));
//            all.removeIf(project-> !strings.contains(project.getDengji()));
//        }
//        if(!(form.getIndustry() == null) && !form.getIndustry().isEmpty() && !form.getIndustry().equals("null")){
//            java.util.List<String> strings = Arrays.asList(form.getIndustry().split(","));
//            all.removeIf(project-> !strings.contains(project.getIndustry()));
//        }
//        java.util.List<Project> list = all.subList((form.getPageno()-1)*pageSize,form.getPageno() * pageSize > all.size() ? all.size() : form.getPageno() * pageSize);
//        return new Result<java.util.List<Project>>(Code.SUCCESS,"查询成功!", list);
//
//    }


    @RequestMapping("search")
    public Result<java.util.List<Project>> searchProject(@RequestBody ProjectSearchForm2 form, HttpServletRequest request){
        int pageSize = 100;
        String whereStr=" ";
        java.util.List<Project> all = new ArrayList<Project>();
        User user = userMapper.getOne(form.getUserid());

        if(user.getUserType() == UserType.Admin){
            if(form.getKeyWord()!=null && !"".equals(form.getKeyWord())){
                all = projectMapper.getAllByKeyWord(form.getKeyWord());
            }else {
                all = projectMapper.getAll();
            }
        }else {
            if(form.getKeyWord()!=null && !"".equals(form.getKeyWord())){
                all = projectMapper.getAllByUserIdAndKeyWord(form.getUserid(),form.getKeyWord());
            }else{
                all = projectMapper.getAllByUserId(form.getUserid());
            }
        }

        java.util.List<Project> list = all.subList((form.getPageno()-1)*pageSize,form.getPageno() * pageSize > all.size() ? all.size() : form.getPageno() * pageSize);
        return new Result<java.util.List<Project>>(Code.SUCCESS,"查询成功!", list);

    }

}
