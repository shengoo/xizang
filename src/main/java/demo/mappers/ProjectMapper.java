package demo.mappers;

import demo.annotations.ProjectSqlProvider;
import demo.models.Project;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ProjectMapper {

    @InsertProvider(type = ProjectSqlProvider.class, method = "insertProject")
    public void insertProject(Project project);

    @Select("SELECT * FROM project WHERE id = #{id}")
    public Project getById(@Param("id")int id);

    @Select("")
    List<Project> getPaging(@Param("from") int from, @Param("count")int count);

    @Select("select count(*) c from project")
    int getTotalNumber();

    @Select("select * from project  order by datecreated desc")
    List<Project> getAll();
    
    @Delete("delete from project where id = #{id}")
    void delete(int id);

    @Select("select * from project where username like '%${username}%' and name like '%${projectname}%' and dengji = #{level} and usertype!='Guest'")
    List<Project> search(@Param("username")String username, @Param("projectname")String projectname, @Param("level")String level);

    @Select("select * from project where phone like '%${phone}%' and name like '%${projectname}%' and dengji = #{level} and usertype='Guest'")
    List<Project> searchGuest(@Param("phone")String phone, @Param("projectname")String projectname, @Param("level")String level);

    @Select("select * from project where userid = #{userid} and (county like '%${whereStr}%' or industry like '%${whereStr}%' or dengji like '%${whereStr}%' or `name` like'%${whereStr}%' ) order by datecreated desc ")
    List<Project> getAllByUserIdAndKeyWord(@Param("userid") int userid, @Param("whereStr")String whereStr);

    @Select("select * from project where userid = #{userid} order by datecreated desc ")
    List<Project> getAllByUserId(@Param("userid") int userid);

    @Select("select * from project where usertype!='Guest'")
    List<Project> getAllUserProject();

    @Select("select * from project where usertype='Guest'")
    List<Project> getAllGuestProject();

    @Select("select * from project where userid = #{id}")
    List<Project> getByUID(int id);

    @Select("select * from project where 1=1 and (county like '%${whereStr}%' or industry like '%${whereStr}%' or dengji like '%${whereStr}%' or `name` like'%${whereStr}%' ) order by datecreated desc ")
    List<Project> getAllByKeyWord(@Param("whereStr")String whereStr);


}
