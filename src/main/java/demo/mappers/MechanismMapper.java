package demo.mappers;

import demo.annotations.MechanismSqlProvider;
import demo.annotations.ProjectSqlProvider;
import demo.models.Mechanism;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MechanismMapper {

    @Select("select * from mechanism")
    List<Mechanism> getAll();

    @Select("SELECT * FROM mechanism LIMIT #{from},#{count}")
    List<Mechanism> getPaging(@Param("from") int from, @Param("count")int count);

    @Select("select count(*) c from mechanism")
    int getTotalNumber();

    @InsertProvider(type = MechanismSqlProvider.class, method = "insertMechanism")
    void insert(Mechanism mechanism);
    
    @Delete("delete from mechanism where id = #{id}")
    void delete(int id);

    @Select("select count(*) c from mechanism where abbr=#{abbr}")
    int existing(String abbr);

    @Select("select count(*) c from mechanism where abbr like '%${abbr}%'")
    int searchCount(@Param("abbr")String abbr);

    @Select("select * from mechanism where abbr like '%${abbr}%' LIMIT #{from},#{count}")
    List<Mechanism> search(@Param("abbr")String abbr,@Param("from") int from, @Param("count")int count);
}
