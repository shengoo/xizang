package demo.mappers;

import demo.services.UserType;
import org.apache.ibatis.annotations.*;

import demo.models.User;

import java.util.Date;
import java.util.List;


public interface UserMapper {

    @Select("SELECT * FROM user WHERE id = #{id}")
    @Results({
            @Result(property = "userType",  column = "usertype", javaType = UserType.class)
    })
    User getOne(int id);

    @Select("SELECT * FROM user")
    List<User> getAll();

    @Select("select * from user where phone = #{phone}") //  and username is NULL
    User getGuest(@Param("phone")String phone);
	
    @Insert("INSERT INTO user (username,password,phone,usertype) VALUES (#{username},#{password},#{phone},#{userType})")
    void addOne(User user);


    @Select("select * from user where username = #{username} and password = #{password}")
    User validateUser(@Param("username")String username,@Param("password")String password);

    @Select("select * from user where phone = #{phone}")
    User getByPhone(@Param("phone")String phone);

    @Select("select * from user where username = #{username}")
    User getByName(@Param("username")String username);

    @Select("select * from user where username = #{username} and phone = #{phone}")
    User getByNameAndPhone(@Param("username")String username,@Param("phone")String phone);

    @Update("update user set password=#{password} where id = #{id}")
    void updatePassword(@Param("id")int id,@Param("password")String password);

    @Update("update user set enabled = #{enabled} where id = #{id}")
    void enableUser(@Param("id") int id, @Param("enabled") boolean enabled);

    @Update("update user set lastvisittime = #{lastvisittime} where id = #{id}")
    void updateLastVisitTime(@Param("id")int id,@Param("lastvisittime")Date lastvisittime);

    @Select("select count(*) c from user")
    int getCount();

    @Select("select count(*) c from user where username like '%${username}%'")
    int searchCount(@Param("username")String username);

    @Select("select * from user LIMIT #{from},#{count}")
    List<User> getPaging(@Param("from") int from, @Param("count")int count);

    @Select("select * from user where username like '%${username}%' LIMIT #{from},#{count}")
    List<User> searchUsername(@Param("username")String username,@Param("from") int from, @Param("count")int count);

    @Update("update user set username=#{username}, password=#{password}, usertype=#{userType}, email=#{email} where id=#{id}")
    void update(User user);
}
