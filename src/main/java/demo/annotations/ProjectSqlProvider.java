package demo.annotations;

import demo.models.Project;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ProjectSqlProvider {
    public String insertProject(Project project){
//        Project project = (Project)map.get("project");
        StringBuilder sql = new StringBuilder("insert into project ");
        //get sql via reflection
        Map<String,String> sqlMap = getAllPropertiesForSql(project);
        //
        sql.append(sqlMap.get("field")).append(sqlMap.get("value"));
        System.out.println(sql.toString());
        return sql.toString();
    }
    //根据传入的对象 基于反射生成两部分sql语句
    private  Map<String,String> getAllPropertiesForSql(Object obj){
    	Map<String,String> map = new HashMap<String,String>();
        if(null == obj) return map;
       StringBuilder filedSql = new StringBuilder("(");
       StringBuilder valueSql = new StringBuilder("value (");
       Field[] fields = obj.getClass().getDeclaredFields();
       for (Field field : fields) {
               // 判断该成员变量上是不是存在Column类型的注解
               if (!field.isAnnotationPresent(Column.class)) {
                   continue;
               }
               Column c = field.getAnnotation(Column.class);// 获取实例
               // 获取元素值
               String columnName = c.name();
               // 如果未指定列名，默认列名使用成员变量名
               if ("".equals(columnName.trim())) {
                   columnName = field.getName();
               }
           filedSql.append(columnName + ",");
           valueSql.append("#{" + field.getName() + "},");
       }
       //remove last ','
       valueSql.deleteCharAt(valueSql.length() - 1);
       filedSql.deleteCharAt(filedSql.length() - 1);
       valueSql.append(") ");
       filedSql.append(") ");
       map.put("field",filedSql.toString());
       map.put("value", valueSql.toString());
       System.out.println("database filed sql: " + filedSql.toString());
       System.out.println("value sql:" + valueSql.toString());
       return map;
    }
}
