package demo.controllers;

import demo.forms.ProjectSearchForm;
import demo.mappers.MechanismMapper;
import demo.mappers.ProjectMapper;
import demo.models.DocumentFile;
import demo.models.Project;
import demo.models.User;
import demo.services.UserType;
import demo.storage.StorageService;
import demo.utils.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("project")
public class ProjectController {
	
	
	private String viewFolder = "project";
	private final StorageService storageService;

	@Autowired
	private ProjectMapper projectMapper;

	@Autowired
	private MechanismMapper mechanismMapper;
	
	@Autowired
    public ProjectController(StorageService storageService) {
        this.storageService = storageService;
    }
	
	@GetMapping("add")
	public String add(Model model){
		model.addAttribute("form", new Project());
		return viewFolder + "/add";
	}

	@PostMapping("add")
	public String addSubmit(@Valid Project form, BindingResult bindingResult, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes){
		if (bindingResult.hasErrors()) {
			model.addAttribute("org.springframework.validation.BindingResult.form", bindingResult);
			model.addAttribute("form", form);
			return viewFolder + "/add";
		}

		if(form.getArea() >= 100 && form.getSum() >= 100){
			form.setDengji("A类");
			form.setXingshi("报告书（A版）");
			form.setShenpi("评审机构先组织查看现场，再组织评审，评审结果报自治区水行政主管部门批准。");
			form.setJiandujiancha("组织实地监督检查。");
			form.setHouxusheji("主体工程初步设计中应有水土保持篇章；施工图设计阶段应进行专项水土保持设计。");
			form.setJianli("1、开展水土保持专项监理\n" +
					"2、验收时提交水土保持监理报告");
			form.setJiance("1、开展水土保持监测\n" +
					"2、按要求提交监测实施方案、季报、年报\n" +
					"3、竣工验收阶段提交监测总结报告");
			form.setSheshiyanshou("开展水土保持设施专项验收前，须经第三方技术评估，验收成果向项目水土保持方案批准机关报告。");
		}
		if(form.getArea() >= 100 || form.getSum() >= 100){
			form.setDengji("A类");
			form.setXingshi("报告书（A版）");
			form.setShenpi("评审机构组织评审，评审结果报自治区水行政主管部门批准。");
			form.setJiandujiancha("组织实地监督检查。");
			form.setHouxusheji("主体工程初步设计中应有水土保持篇章；施工图设计阶段应进行专项水土保持设计。");
			form.setJianli("1、开展水土保持专项监理\n" +
					"2、验收时提交水土保持监理报告");
			form.setJiance("1、开展水土保持监测\n" +
					"2、按要求提交监测实施方案、季报、年报\n" +
					"3、竣工验收阶段提交监测总结报告");
			form.setSheshiyanshou("开展水土保持设施专项验收前，须经第三方技术评估，验收成果向项目水土保持方案批准机关报告。");
		}

		if((form.getArea() >= 50 && form.getArea() < 100) || (form.getSum()  >= 50 && form.getSum() < 100)){
			form.setDengji("B类");
			form.setXingshi("报告书（B版）");
			form.setShenpi("评审机构组织评审，评审结果报自治区水行政主管部门批准。");
			form.setJiandujiancha("组织实地检查、会议检查、书面检查等方式进行监督检查。");
			form.setHouxusheji("主体工程初步设计中应有水土保持篇章");
			form.setJianli("1、主体工程监理单位中应有经注册的水土保持监理工程师；\n" +
					"2、验收时提交水土保持监理报告。");
			form.setJiance("1、开展水土保持监测\n" +
					"2、竣工验收阶段提交监测总结报告；");
			form.setSheshiyanshou("开展水土保持设施专项验收，验收成果向项目水土保持方案批批机关报告。");
		}

		if((form.getArea() >= 20 && form.getArea() < 50) || (form.getSum()  >= 20 && form.getSum() < 50)){
			form.setDengji("B类");
			form.setXingshi("报告书（B版）");
			form.setShenpi("评审机构组织评审，评审结果报自治区水行政主管部门批准。");
			form.setJiandujiancha("组织实地检查、会议检查、书面检查等方式进行监督检查。");
			form.setHouxusheji("主体工程初步设计中应有水土保持篇章");
			form.setJianli("1、主体工程监理单位中应有经注册的水土保持监理工程师；\n" +
					"2、验收时提交水土保持监理报告。");
			form.setJiance("1、开展水土保持监测\n" +
					"2、竣工验收阶段提交监测总结报告；");
			form.setSheshiyanshou("开展水土保持设施专项验收，验收成果向项目水土保持方案批批机关报告。");
		}

		if((form.getArea() >= 1 && form.getArea() < 20) || (form.getSum()  >= 1 && form.getSum() < 20)){
			form.setDengji("C类");
			form.setXingshi("报告书（C版）");
			form.setShenpi("地（市）水行政主管部门审批，跨地（市）的项目由自治区水行政主管部门审批。");
			form.setJiandujiancha("地（市）、县（区）水行政主管部门按属地原则，组织实施水土保持监督检查。");
			form.setHouxusheji("主体工程初步设计中应有水土保持内容");
			form.setJianli("主体工程监理中应有水土保持监理相关内容");
			form.setJiance("鼓励开展水土保持监测");
			form.setSheshiyanshou("与主体工程一并验收，验收成果由验收主持单位和建设单位自行存档备查。");
		}
		if(form.getArea() < 1 || form.getSum() < 1){
			form.setDengji("C类");
			form.setXingshi("报告表");
			form.setShenpi("地（市）水行政主管部门审批，跨地（市）的项目由自治区水行政主管部门审批。");
			form.setJiandujiancha("地（市）、县（区）水行政主管部门按属地原则，组织实施水土保持监督检查。");
			form.setHouxusheji("主体工程初步设计中应有水土保持内容");
			form.setJianli("主体工程监理中应有水土保持监理相关内容");
			form.setJiance("鼓励开展水土保持监测");
			form.setSheshiyanshou("与主体工程一并验收，验收成果由验收主持单位和建设单位自行存档备查。");
		}

		form.setDatecreated(new Date());
		HttpSession session = request.getSession();
		if(session.getAttribute("USER") != null) {
			User user = (User) session.getAttribute("USER");
			form.setUserid(user.getId());
			form.setUsername(user.getUsername());
			form.setPhone(user.getPhone());
			form.setUsertype(user.getUserType());
		}
		projectMapper.insertProject(form);

		System.out.println(form.toString());
		redirectAttributes.addFlashAttribute("project", form);
		return "redirect:/" + viewFolder + "/result";
	}

	@RequestMapping("result")
	public String result(Model model){
//		Project project = projectMapper.getById(17);
//		Project project = new Project();
//		System.out.println(project.toString());
//		model.addAttribute("project", project);
		return viewFolder + "/result";
		
	}
	
	@RequestMapping("help")
	public String help(Model model,@RequestParam(required=false,value="pageno",defaultValue="1") int pageno){
		int pageSize = 10;
		int total = mechanismMapper.getTotalNumber();
		if(total > 0){
//			model.addAttribute("list",mechanismMapper.getAll());
			model.addAttribute("list",mechanismMapper.getPaging((pageno-1)*pageSize,pageSize));
			model.addAttribute("pageno", pageno);
			model.addAttribute("pagecount", (int)Math.ceil((double)total / pageSize));
			model.addAttribute("total", total);
		}
		return viewFolder + "/help";
	}
	
	
	
	@RequestMapping("documents")
	public String documents(Model model){
		DocumentFile[] files = new DocumentFile[11];
		files[0] = new DocumentFile();
		files[0].name = "中华人民共和国水土保持法";
		files[0].filename = "11.pdf";
		files[1] = new DocumentFile();
		files[1].name = "西藏自治区实施《中华人民共和国水土保持法》办法";
		files[1].filename = "9.pdf";
		files[2] = new DocumentFile();
		files[2].name = "国家级两区划分名录--西藏";
		files[2].filename = "2.pdf";
		files[3] = new DocumentFile();
		files[3].name = "西藏自治区水土流失重点防治区划分情况";
		files[3].filename = "10.pdf";
		files[4] = new DocumentFile();
		files[4].name = "关于调整补偿费收费标准的通知";
		files[4].filename = "1.pdf";
		files[5] = new DocumentFile();
		files[5].name = "西藏自治区生产建设项目水土保持分类管理办法（试行）-20171121";//"西藏自治区生产建设项目水土保持分类管理办法（试行）";
		files[5].filename = "8.pdf";
		files[6] = new DocumentFile();
		files[6].name = "西藏分类管理表（20171117）";//"西藏自治区区级生产建设项目水土保持分类管理表";
		files[6].filename = "7.pdf";
		files[7] = new DocumentFile();
		files[7].name = "水土保持方案报告书（A版）提纲";
		files[7].filename = "4.pdf";
		files[8] = new DocumentFile();
		files[8].name = "水土保持方案报告书（B版）提纲";
		files[8].filename = "5.pdf";
		files[9] = new DocumentFile();
		files[9].name = "水土保持方案报告书（C版）提纲";
		files[9].filename = "6.pdf";
		files[10] = new DocumentFile();
		files[10].name = "水土保持方案报告表--模版";
		files[10].filename = "3.pdf";


		model.addAttribute("files",files);
		return viewFolder + "/documents";
	}
	
	@GetMapping("manage")
	public String manage(Model model){
		model.addAttribute("form",new ProjectSearchForm());
		return viewFolder + "/manage";
	}

	@PostMapping("manage")
	public String managePost(ProjectSearchForm form, Model model, HttpServletRequest request){
		System.out.println(form.toString());
		model.addAttribute("form",form);
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("USER");

		int pageSize = 10;
		List<Project> all;// = projectMapper.getAll();
		if(user.getUserType() == UserType.Guest || user.getUserType() == UserType.User){
			System.out.println("get by user");
			all = projectMapper.getByUID(user.getId());
		}else {
			System.out.println("get all");
			all = projectMapper.getAll();
		}
		if(!form.getAreaFrom().isEmpty()){
			float areaFrom = Float.parseFloat(form.getAreaFrom());
			all.removeIf(project -> project.getArea() < areaFrom);
		}
		if(!form.getAreaTo().isEmpty()){
			float areaTo = Float.parseFloat(form.getAreaTo());
			all.removeIf(project -> project.getArea() >= areaTo);
		}
		if(form.getDateFrom()!=null){
			all.removeIf(project -> !project.getDatecreated().after(form.getDateFrom()));
		}
		if(form.getDateTo()!=null){
			Date tomorrow = DateHelper.getTomorrow(form.getDateTo());
			all.removeIf(project -> !project.getDatecreated().before(tomorrow));
		}
		if(!(form.getCounty() == null) && !form.getCounty().isEmpty() && !form.getCounty().equals("null")){
			String[] countys = form.getCounty().split(",");
			all.removeIf(project-> project.getCounty() == null || !Arrays.stream(countys).parallel().anyMatch(project.getCounty()::contains));
		}
		if(!(form.getDengji() == null) && !form.getDengji().isEmpty() && !form.getDengji().equals("null")){
			List<String> strings = Arrays.asList(form.getDengji().split(","));
			all.removeIf(project-> !strings.contains(project.getDengji()));
		}
		if(!(form.getIndustry() == null) && !form.getIndustry().isEmpty() && !form.getIndustry().equals("null")){
			List<String> strings = Arrays.asList(form.getIndustry().split(","));
			all.removeIf(project-> !strings.contains(project.getIndustry()));
		}
		model.addAttribute("list",all.subList((form.getPageno()-1)*pageSize,form.getPageno() * pageSize > all.size() ? all.size() : form.getPageno() * pageSize));
		model.addAttribute("pageno", form.getPageno());
		model.addAttribute("pagecount", (int)Math.ceil((double)all.size() / pageSize));
		model.addAttribute("total", all.size());
		model.addAttribute("showresult", 1);


		session.setAttribute("projects", all);

		return viewFolder + "/manage";
	}
	
	@GetMapping("download/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadFile(@PathVariable String filename) {

		
        Resource file = storageService.loadAsResource(filename);
		final String encodedFileName = encodeFilename(filename); 
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+encodedFileName+"\"")
                .body(file);
    }
	
	
	@GetMapping("show/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> showFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity
                .ok()
                .body(file);
    }
	
	@PostMapping("delete")
	public String delete(@RequestParam int id,RedirectAttributes redirectAttributes) {
		System.out.println("delete id : " + id);
		projectMapper.delete(id);
		redirectAttributes.addAttribute("showresult", 1);
		redirectAttributes.addFlashAttribute("message","删除成功.");
		return "redirect:/" + viewFolder + "/manage";
	}
	
	private String encodeFilename(final String filename)
	{
	  try{        
	    URI uri = new URI(null, null, filename, null);      
	    String encodedName = uri.toASCIIString(); 
	    return encodedName;
	  }
	  catch(URISyntaxException ex){
	      return filename;
	  }
	}

}
