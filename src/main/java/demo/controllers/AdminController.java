package demo.controllers;

import demo.forms.UserLogForm;
import demo.forms.UserSearchForm;
import demo.mappers.MechanismMapper;
import demo.mappers.ProjectMapper;
import demo.mappers.UserMapper;
import demo.models.Mechanism;
import demo.models.Project;
import demo.utils.DateHelper;
import demo.utils.PasswordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("admin")
public class AdminController {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private MechanismMapper mechanismMapper;

	@Autowired
	private ProjectMapper projectMapper;

	private String viewFolder = "admin";
	int pageSize = 10;

	
	@RequestMapping("userlog")
	public String userlog(Model model, UserLogForm form, HttpServletRequest request){
		model.addAttribute("form", form);
		List<Project> all = projectMapper.search(form.getUsername(),form.getProjectname(),form.getLevel());
		if(form.getDateFrom()!=null){
			all.removeIf(project -> project.getDatecreated().before(form.getDateFrom()));
		}
		if(form.getDateTo() != null){
			Date tomorrow = DateHelper.getTomorrow(form.getDateTo());
			all.removeIf(project -> project.getDatecreated().after(tomorrow));
		}
		model.addAttribute("list",all.subList((form.getPageno()-1)*pageSize,form.getPageno() * pageSize > all.size() ? all.size() : form.getPageno() * pageSize));
		model.addAttribute("pageno", form.getPageno());
		model.addAttribute("pagecount", (int)Math.ceil((double)all.size() / pageSize));
		model.addAttribute("total", all.size());

		HttpSession session = request.getSession();
		session.setAttribute("userproject",all);

		return viewFolder + "/userlog";
	}
	@RequestMapping("publiclog")
	public String publiclog(Model model, UserLogForm form, HttpServletRequest request){
		model.addAttribute("form", form);
		List<Project> all = projectMapper.searchGuest(form.getUsername(),form.getProjectname(),form.getLevel());
		if(form.getDateFrom()!=null){
			all.removeIf(project -> project.getDatecreated().before(form.getDateFrom()));
		}
		if(form.getDateTo() != null){
			Date tomorrow = DateHelper.getTomorrow(form.getDateTo());
			all.removeIf(project -> project.getDatecreated().after(tomorrow));
		}
		model.addAttribute("list",all.subList((form.getPageno()-1)*pageSize,form.getPageno() * pageSize > all.size() ? all.size() : form.getPageno() * pageSize));
		model.addAttribute("pageno", form.getPageno());
		model.addAttribute("pagecount", (int)Math.ceil((double)all.size() / pageSize));
		model.addAttribute("total", all.size());

		HttpSession session = request.getSession();
		session.setAttribute("guestproject",all);

		return viewFolder + "/publiclog";
	}

	@PostMapping("deleteuserproject")
	public String deleteuserproject(@RequestParam int id,RedirectAttributes redirectAttributes){
		projectMapper.delete(id);
		redirectAttributes.addFlashAttribute("message","删除成功.");
		return "redirect:/" + viewFolder + "/userlog";
	}

	@PostMapping("deletepublicproject")
	public String deletepublicproject(@RequestParam int id,RedirectAttributes redirectAttributes){
		projectMapper.delete(id);
		redirectAttributes.addFlashAttribute("message","删除成功.");
		return "redirect:/" + viewFolder + "/publiclog";
	}

	@GetMapping("insinfo")
	public String insinfo(Model model,@RequestParam(required=false,value="pageno",defaultValue="1") int pageno){
		if (!model.containsAttribute("form")) {
			model.addAttribute("form", new Mechanism());
		}
		int total = mechanismMapper.getTotalNumber();
		if(total > 0){
            model.addAttribute("list",mechanismMapper.getPaging((pageno-1)*pageSize,pageSize));
            model.addAttribute("pageno", pageno);
            model.addAttribute("pagecount", (int)Math.ceil((double)total / pageSize));
            model.addAttribute("total", total);
		}
		return viewFolder + "/insinfo";
	}

	@PostMapping("addmech")
	public String addMochanism(@Valid Mechanism form, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes){
		String rejectUrl = "redirect:/" + viewFolder + "/insinfo";
		System.out.println(bindingResult.hasErrors());
		if (bindingResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", bindingResult);
			redirectAttributes.addFlashAttribute("form", form);
			return rejectUrl;
		}
		int count = mechanismMapper.existing(form.getAbbr());
		if(count > 0){
			bindingResult.rejectValue("abbr", "error.abbr", "机构" + form.getAbbr() + "已存在");
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", bindingResult);
			redirectAttributes.addFlashAttribute("form", form);
			return rejectUrl;
		}
		form.setDatecreated(new Date());
		mechanismMapper.insert(form);
		System.out.println(form.toString());
		return rejectUrl;
	}
	
	@PostMapping("deletemech")
	public String deleteMechanism(@RequestParam int id,RedirectAttributes redirectAttributes) {
		System.out.println("delete id : " + id);
		mechanismMapper.delete(id);
		redirectAttributes.addFlashAttribute("message","删除成功.");
		return "redirect:/" + viewFolder + "/insinfo";
	}

	@PostMapping("searchmech")
	public String searchMech(Model model,Mechanism form){
		System.out.println(form.toString());
		if (!model.containsAttribute("form")) {
		model.addAttribute("form", new Mechanism());
	}
		int total = mechanismMapper.searchCount(form.getAbbr());
		if(total > 0){
			model.addAttribute("list",mechanismMapper.search(form.getAbbr(),0,pageSize));
			model.addAttribute("pageno", 1);
			model.addAttribute("pagecount", (int)Math.ceil((double)total / pageSize));
		}
		model.addAttribute("total", total);
		return viewFolder + "/insinfo";
	}

	@GetMapping("userinfo")
	public String userinfo(Model model){
		model.addAttribute("form",new UserSearchForm());
		int pageSize = 10;
		int pageno = 1;
		int total = userMapper.getCount();
		if(total > 0){
			model.addAttribute("list",userMapper.getPaging((pageno - 1)*pageSize,pageSize));
			model.addAttribute("pageno", pageno);
			model.addAttribute("pagecount", (int)Math.ceil((double)total / pageSize));
		}
        model.addAttribute("total", total);
		return viewFolder + "/userinfo";
	}

	@PostMapping("userinfo")
	public String userinfoPost(Model model,UserSearchForm form){
		form.setQ(form.getQ().trim());
		System.out.println(form.toString());
		model.addAttribute("form",form);
		int pageSize = 10;
		int total = userMapper.searchCount(form.getQ());
		if(total > 0){
			model.addAttribute("list",userMapper.searchUsername(form.getQ(),(form.getPageno() - 1)*pageSize,pageSize));
			model.addAttribute("pageno", form.getPageno());
			model.addAttribute("pagecount", (int)Math.ceil((double)total / pageSize));
		}
        model.addAttribute("total", total);

		return viewFolder + "/userinfo";
	}

	@PostMapping("resetuser")
	public String resetUserPasswordPost(@RequestParam int id, RedirectAttributes redirectAttributes){
		resetUserPassword(id,PasswordHelper.encryptPassword("111111"));
		redirectAttributes.addFlashAttribute("message","重置成功.");
		return "redirect:/" + viewFolder + "/userinfo";
	}

	@PostMapping("disableuser")
	public String disableUserPost(@RequestParam int id, RedirectAttributes redirectAttributes){
		disableUser(id);
		redirectAttributes.addFlashAttribute("message","注销成功.");
		return "redirect:/" + viewFolder + "/userinfo";
	}

	@PostMapping("enableuser")
	public String enableUserPost(@RequestParam int id, RedirectAttributes redirectAttributes){
		enableUser(id);
		redirectAttributes.addFlashAttribute("message","启用成功.");
		return "redirect:/" + viewFolder + "/userinfo";
	}
	
	private void resetUserPassword(int id, String password){
		String encrypedPassword = PasswordHelper.encryptPassword(password);
		userMapper.updatePassword(id,encrypedPassword);
	}

	private void disableUser(int id){
		userMapper.enableUser(id,false);
	}

	private void enableUser(int id){
		userMapper.enableUser(id,true);
	}

	public String reject(Model model,Object bindingResult, Object form, String url){
		model.addAttribute("org.springframework.validation.BindingResult.form", bindingResult);
		model.addAttribute("form", form);
		return url;
	}
}
