package demo.controllers;

import demo.forms.ForgetForm;
import demo.forms.RegisterForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.forms.PublicLogin;
import demo.forms.UserLogin;

@Controller
@RequestMapping("/")
public class IndexController {

    @RequestMapping("/")
    public String index(Model model) {
    	if (!model.containsAttribute("publicLogin")) {
            model.addAttribute("publicLogin", new PublicLogin());
        }
    	if (!model.containsAttribute("userLogin")) {
            model.addAttribute("userLogin", new UserLogin());
        }
    	if (!model.containsAttribute("action")) {
            model.addAttribute("action", "userLogin");
        }
        if (!model.containsAttribute("register")) {
            model.addAttribute("register", new RegisterForm());
        }
        if (!model.containsAttribute("forget")) {
            model.addAttribute("forget", new ForgetForm());
        }
        return "index";
    }

}
