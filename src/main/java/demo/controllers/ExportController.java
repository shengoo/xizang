package demo.controllers;

import demo.excel.MechanismExcelView;
import demo.excel.ProjectExcelView;
import demo.excel.UserExcelView;
import demo.forms.ProjectSearchForm;
import demo.mappers.MechanismMapper;
import demo.mappers.ProjectMapper;
import demo.mappers.UserMapper;
import demo.models.Mechanism;
import demo.models.Project;
import demo.models.User;
import demo.services.UserType;
import demo.utils.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("export")
public class ExportController {

    @Autowired
    UserMapper userMapper;

    @Autowired
    MechanismMapper mechanismMapper;

    @Autowired
    ProjectMapper projectMapper;

    @RequestMapping("mechan")
    public ModelAndView exportMechanism(){
        List<Mechanism> list = mechanismMapper.getAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("members", list);
        map.put("name", "机构信息");
        MechanismExcelView excelView = new MechanismExcelView();
        return new ModelAndView(excelView, map);

    }

    @RequestMapping("user")
    public ModelAndView exportUsers(){
        List<User> list = userMapper.getAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("members", list);
        map.put("name", "用户信息");
        UserExcelView excelView = new UserExcelView();
        return new ModelAndView(excelView, map);
    }

    @RequestMapping("project")
    public ModelAndView exportProject(){
        List<Project> list = projectMapper.getAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("members", list);
        map.put("name", "项目信息");
        ProjectExcelView excelView = new ProjectExcelView();
        return new ModelAndView(excelView, map);
    }

    @RequestMapping("userproject")
    public ModelAndView exportUserProject(HttpServletRequest request){
        HttpSession session = request.getSession();
        List<Project> list;
        if(session.getAttribute("userproject") != null){
            list = (List<Project>) session.getAttribute("userproject");
        }else{
            list = projectMapper.getAllUserProject();
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("members", list);
        map.put("name", "项目信息");
        ProjectExcelView excelView = new ProjectExcelView();
        return new ModelAndView(excelView, map);
    }

    @RequestMapping("guestproject")
    public ModelAndView exportGuestProject(HttpServletRequest request){
        HttpSession session = request.getSession();
        List<Project> list;
        if(session.getAttribute("guestproject") != null){
            list = (List<Project>) session.getAttribute("guestproject");
        }else{
            list = projectMapper.getAllGuestProject();
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("members", list);
        map.put("name", "项目信息");
        ProjectExcelView excelView = new ProjectExcelView();
        return new ModelAndView(excelView, map);
    }

    @RequestMapping("searchproject")
    public ModelAndView searchproject(ProjectSearchForm form, HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("USER");
        List<Project> all;// = projectMapper.getAll();
        if(user.getUserType() == UserType.Guest || user.getUserType() == UserType.User){
            System.out.println("get by user");
            all = projectMapper.getByUID(user.getId());
        }else {
            System.out.println("get all");
            all = projectMapper.getAll();
        }
        if(!form.getAreaFrom().isEmpty()){
            float areaFrom = Float.parseFloat(form.getAreaFrom());
            all.removeIf(project -> project.getArea() < areaFrom);
        }
        if(!form.getAreaTo().isEmpty()){
            float areaTo = Float.parseFloat(form.getAreaTo());
            all.removeIf(project -> project.getArea() >= areaTo);
        }
        if(form.getDateFrom()!=null){
            all.removeIf(project -> !project.getDatecreated().after(form.getDateFrom()));
        }
        if(form.getDateTo()!=null){
            Date tomorrow = DateHelper.getTomorrow(form.getDateTo());
            all.removeIf(project -> !project.getDatecreated().before(tomorrow));
        }
        if(!(form.getCounty() == null) && !form.getCounty().isEmpty() && !form.getCounty().equals("null")){
            String[] countys = form.getCounty().split(",");
            all.removeIf(project-> project.getCounty() == null || !Arrays.stream(countys).parallel().anyMatch(project.getCounty()::contains));
        }
        if(!(form.getDengji() == null) && !form.getDengji().isEmpty() && !form.getDengji().equals("null")){
            List<String> strings = Arrays.asList(form.getDengji().split(","));
            all.removeIf(project-> !strings.contains(project.getDengji()));
        }
        if(!(form.getIndustry() == null) && !form.getIndustry().isEmpty() && !form.getIndustry().equals("null")){
            List<String> strings = Arrays.asList(form.getIndustry().split(","));
            all.removeIf(project-> !strings.contains(project.getIndustry()));
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("members", all);
        map.put("name", "项目信息");
        ProjectExcelView excelView = new ProjectExcelView();
        return new ModelAndView(excelView, map);
    }


}
