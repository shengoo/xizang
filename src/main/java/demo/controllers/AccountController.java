package demo.controllers;

import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import demo.forms.ForgetForm;
import demo.forms.RegisterForm;
import demo.services.UserType;
import demo.utils.PasswordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import demo.mappers.UserMapper;
import demo.forms.PublicLogin;
import demo.models.User;
import demo.forms.UserLogin;
import demo.services.SmsService;
import demo.services.SmsType;

@Controller
@RequestMapping("account")
public class AccountController {

	@Autowired
	private SmsService smsService;

	@Autowired
	private UserMapper userMapper;
	
	@GetMapping("userlogin")
	public String userlogin(){
		return "account/userlogin";
	}
	
	@PostMapping("userlogin")
	public ModelAndView userloginPost(@Valid UserLogin userLogin,BindingResult bindingResult, RedirectAttributes redirectAttributes,HttpServletRequest request){
		if (bindingResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.userLogin", bindingResult);
			redirectAttributes.addFlashAttribute("userLogin", userLogin);
			redirectAttributes.addFlashAttribute("action", "userLogin");
			return new ModelAndView("redirect:/");//new ModelAndView(viewName, modelName, modelObject)
        }
        User user = userMapper.validateUser(userLogin.getUsername(),PasswordHelper.encryptPassword(userLogin.getPassword()));
		if(user == null){
			bindingResult.rejectValue("password", "error.password", "密码错误");
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.userLogin", bindingResult);
			redirectAttributes.addFlashAttribute("userLogin", userLogin);
			redirectAttributes.addFlashAttribute("action", "userLogin");
			return new ModelAndView("redirect:/");
		}
		if(!user.isEnabled()){
			bindingResult.rejectValue("username", "error.username", "用户被禁用");
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.userLogin", bindingResult);
			redirectAttributes.addFlashAttribute("userLogin", userLogin);
			redirectAttributes.addFlashAttribute("action", "userLogin");
			return new ModelAndView("redirect:/");
		}
		userMapper.updateLastVisitTime(user.getId(),new Date());
        HttpSession session = request.getSession();
        session.setAttribute("USER",user);
		return new ModelAndView("redirect:/project/add");
		
	}
	
	@GetMapping("publiclogin")
	public String publiclogin(){
		return "account/publiclogin";
	}
	
	@PostMapping("sendcode")
	public ResponseEntity sendcode(@RequestParam("phone") String phone, HttpServletRequest request){
		StringBuilder code = new StringBuilder();
	    Random random = new Random();
	    // 生成6位验证码
	    for (int i = 0; i < 6; i++) {
	        code.append(String.valueOf(random.nextInt(10)));
	    }
	    HttpSession session = request.getSession();
	    session.setAttribute("VALIDATE_PHONE", phone);
	    session.setAttribute("VALIDATE_PHONE_CODE", code.toString());
	    session.setAttribute("SEND_CODE_TIME", new Date().getTime());
	    String smsText = "您的登录验证码是:"+code;
	    System.out.println(phone + ' ' + smsText);
	    boolean result = smsService.sendSms(SmsType.Login, phone, code.toString());
	    if(result) {
	    		return ResponseEntity.status(HttpStatus.OK).body(null);
	    }else {
	    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
	    }
		
	}
	
	@PostMapping("sendcode2")
	public ResponseEntity sendcode2(@RequestParam("phone") String phone, HttpServletRequest request){
		StringBuilder code = new StringBuilder();
	    Random random = new Random();
	    // 生成6位验证码
	    for (int i = 0; i < 6; i++) {
	        code.append(String.valueOf(random.nextInt(10)));
	    }
	    HttpSession session = request.getSession();
	    session.setAttribute("VALIDATE_PHONE", phone);
	    session.setAttribute("VALIDATE_PHONE_CODE", code.toString());
	    session.setAttribute("SEND_CODE_TIME", new Date().getTime());
	    String smsText = "您的注册验证码是:"+code;
	    System.out.println(phone + ' ' + smsText);
	    boolean result = smsService.sendSms(SmsType.Register, phone, code.toString());
	    if(result) {
	    		return ResponseEntity.status(HttpStatus.OK).body(null);
	    }else {
	    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
	    }
		
	}

	@PostMapping("sendcode3")
	public ResponseEntity sendcode3(@RequestParam("phone") String phone, HttpServletRequest request){
		StringBuilder code = new StringBuilder();
		Random random = new Random();
		// 生成6位验证码
		for (int i = 0; i < 6; i++) {
			code.append(String.valueOf(random.nextInt(10)));
		}
		HttpSession session = request.getSession();
		session.setAttribute("VALIDATE_PHONE", phone);
		session.setAttribute("VALIDATE_PHONE_CODE", code.toString());
		session.setAttribute("SEND_CODE_TIME", new Date().getTime());
		String smsText = "您的找回密码验证码是:"+code;
		System.out.println(phone + ' ' + smsText);
		boolean result = smsService.sendSms(SmsType.Register, phone, code.toString());
		if(result) {
			return ResponseEntity.status(HttpStatus.OK).body(null);
		}else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}

	}

	@RequestMapping("checkname")
	public ResponseEntity checkname(@RequestParam String username){
		User user = userMapper.getByName(username);
		if(user == null) {
			return ResponseEntity.status(HttpStatus.OK).body(null);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}

    @RequestMapping("checkexistingname")
    public ResponseEntity checkexistingname(@RequestParam String username){
        User user = userMapper.getByName(username);
        if(user != null) {
            return ResponseEntity.status(HttpStatus.OK).body(null);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

	@RequestMapping("checkcode")
    public ResponseEntity checkcode(@RequestParam String code, HttpServletRequest request){
	    HttpSession session = request.getSession();
	    if(session.getAttribute("VALIDATE_PHONE_CODE") == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        String sentCode = (String) session.getAttribute("VALIDATE_PHONE_CODE");
	    if(code.equals(sentCode)){
            return ResponseEntity.status(HttpStatus.OK).body(null);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }
	
	@PostMapping("publiclogin")
	public ModelAndView publicLoginPost(@Valid PublicLogin publicLogin, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request){
		System.out.println(publicLogin);
		
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult);
//			model.addAttribute("publiclogin", publicLogin);
//			redirectAttributes.addAttribute("publiclogin", publicLogin);
			
//            return new ModelAndView("/index");
//			return "redirect:/";
//			return MvcUriComponentsBuilder.fromMethodName(IndexController.class,"index", publicLogin).build().toString();

			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.publicLogin", bindingResult);
//			return new ModelAndView("redirect:/");
			
			redirectAttributes.addFlashAttribute("publicLogin", publicLogin);
			redirectAttributes.addFlashAttribute("action", "publicLogin");
			return new ModelAndView("redirect:/");//new ModelAndView(viewName, modelName, modelObject)
        }
		
		HttpSession session = request.getSession();
		String phone = (String) session.getAttribute("VALIDATE_PHONE");
		String code = (String) session.getAttribute("VALIDATE_PHONE_CODE");
		
		System.out.println(phone + " " + publicLogin.getPhone() + " " + code + " " + publicLogin.getCode());
		
		if(!phone.equals(publicLogin.getPhone()) || !code.equals(publicLogin.getCode())){
			bindingResult.rejectValue("code", "error.code", "请输入正确的验证码");
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.publicLogin", bindingResult);
			redirectAttributes.addFlashAttribute("publicLogin", publicLogin);
			redirectAttributes.addFlashAttribute("action", "publicLogin");
			return new ModelAndView("redirect:/");
		}

		User user = userMapper.getGuest(publicLogin.getPhone());
		if(user == null){
			user = new User();
			user.setPhone(publicLogin.getPhone());
			user.setUserType(UserType.Guest);
			userMapper.addOne(user);
			user = userMapper.getGuest(publicLogin.getPhone());
		}
		userMapper.updateLastVisitTime(user.getId(),new Date());

		session.setAttribute("USER",user);
		


		
		return new ModelAndView("redirect:/project/add");
	}

	
	@GetMapping("register")
	public String register(Model model){
//		User user = userMapper.getOne(10);
//		System.out.println(user.toString());
		model.addAttribute("form", new RegisterForm());
		return "account/register2";
	}

	@RequestMapping("logout")
	public String logout(HttpServletRequest request){
		request.getSession().invalidate();
		return "redirect:/";
	}

	@PostMapping("register")
	public String registerPost(@Valid RegisterForm form, BindingResult bindingResult, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String rejectUrl = "redirect:/";
		if (bindingResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.register", bindingResult);
			redirectAttributes.addFlashAttribute("register", form);
			return rejectUrl;
		}
		HttpSession session = request.getSession();
		String phone = (String) session.getAttribute("VALIDATE_PHONE");
		String code = (String) session.getAttribute("VALIDATE_PHONE_CODE");
		if(!phone.equals(form.getPhone()) || !code.equals(form.getCode())){
			bindingResult.rejectValue("code", "error.code", "请输入正确的验证码");
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.register", bindingResult);
			redirectAttributes.addFlashAttribute("register", form);
			return rejectUrl;
		}

		User existing = userMapper.getByName(form.getUsername());
		if(existing != null) {
			bindingResult.rejectValue("username", "username.code", "用户名已注册");
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.register", bindingResult);
			redirectAttributes.addFlashAttribute("register", form);
			return rejectUrl;
		}
		User checkPhone = userMapper.getByPhone(form.getPhone());

		if(checkPhone != null && checkPhone.getUserType() != UserType.Guest) {
			bindingResult.rejectValue("phone", "phone.code", "手机号已注册");
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.register", bindingResult);
			redirectAttributes.addFlashAttribute("register", form);
			return rejectUrl;
		}



		User user = new User();
		user.setPhone(form.getPhone());
		user.setUserType(UserType.User);
		user.setUsername(form.getUsername());
		user.setPassword(PasswordHelper.encryptPassword(form.getPassword()));
		user.setEmail(form.getEmail());
		if(checkPhone!= null){
			user.setId(checkPhone.getId());
			user.setLastvisittime(new Date());
			userMapper.update(user);
		}else {
			userMapper.addOne(user);
		}

		redirectAttributes.addFlashAttribute("registerSuccess", true);
		return rejectUrl;
	}
	
	@GetMapping("forget")
	public String forget(Model model) {
		model.addAttribute("form", new ForgetForm());
		return "account/forget";
	}

	@PostMapping("forget")
	public String forgetSubmit(@Valid ForgetForm form, BindingResult bindingResult, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes){
		String rejectUrl = "redirect:/";
		if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.forget", bindingResult);
            redirectAttributes.addFlashAttribute("forget", form);
			return rejectUrl;
		}

        HttpSession session = request.getSession();
        String phone = (String) session.getAttribute("VALIDATE_PHONE");
        String code = (String) session.getAttribute("VALIDATE_PHONE_CODE");
        if(!phone.equals(form.getPhone()) || !code.equals(form.getCode())){
            bindingResult.rejectValue("code", "error.code", "请输入正确的验证码");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.forget", bindingResult);
            redirectAttributes.addFlashAttribute("forget", form);
            return rejectUrl;
        }

		if(!form.getPassword().equals(form.getRepeatPassword())){
			bindingResult.rejectValue("repeatPassword", "repeatPassword.code", "两次密码不同");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.forget", bindingResult);
            redirectAttributes.addFlashAttribute("forget", form);
			return rejectUrl;
		}

		User user = userMapper.getByName(form.getUsername());
		if(user == null){
			bindingResult.rejectValue("username", "username.code", "用户名未注册");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.forget", bindingResult);
            redirectAttributes.addFlashAttribute("forget", form);
            return rejectUrl;
		}


		if(!user.getPhone().equals(form.getPhone())){
			bindingResult.rejectValue("phone", "phone.code", "手机号不符");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.forget", bindingResult);
            redirectAttributes.addFlashAttribute("forget", form);
            return rejectUrl;
		}

		userMapper.updatePassword(user.getId(), PasswordHelper.encryptPassword(form.getPassword()));

		redirectAttributes.addFlashAttribute("forgetSuccess", true);

		return rejectUrl;
	}

	public String reject(Model model,Object bindingResult, Object form, String url){
		model.addAttribute("org.springframework.validation.BindingResult.form", bindingResult);
		model.addAttribute("form", form);
		return url;
	}



}
