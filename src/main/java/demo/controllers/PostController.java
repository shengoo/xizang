package demo.controllers;

import demo.models.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("test")
public class PostController {

    @RequestMapping("")
    public List<User> test(@RequestBody User user){
        List<User> users = new ArrayList<User>();
        users.add(user);
        users.add(user);
        return users;
    }

}
