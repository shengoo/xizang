package demo.storage;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;

public interface StorageService {
	void init();

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

}
