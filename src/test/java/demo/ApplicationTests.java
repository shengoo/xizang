package demo;

import demo.mappers.ProjectMapper;
import demo.mappers.UserMapper;
import demo.models.Project;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	@Autowired
	UserMapper userMapper;

	@Autowired
	ProjectMapper projectMapper;

	@Test
	public void contextLoads() {
//		System.out.println(userMapper.searchCount("zh"));
	}

//	@Test
	public void testProjectSearch(){
		List<Project> list = projectMapper.search("","","A类");
		System.out.println(list.size());
	}

//	@Test
	public void testGuestProjectSearch(){
		List<Project> list = projectMapper.searchGuest("","","A类");
		System.out.println(list.size());
	}

}
