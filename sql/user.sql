﻿SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS  `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT '',
  `password` varchar(20) DEFAULT '',
  `phone` varchar(20) DEFAULT '',
  `usertype` varchar(20) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT '1',
  `lastvisittime` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

insert into `user`(`id`,`username`,`password`,`phone`,`usertype`,`email`,`enabled`,`lastvisittime`) values
('10','a','a','13111111111','Admin','test@12.com','1','2017-11-15'),
('11','b','111111','123','User',null,'0','2017-11-09'),
('13','zhang','111111','18811550521','User',null,'1','2017-11-13'),
('14','zhanghaifeng','123456','18612031189','User',null,'1',null),
('15',null,null,'13222222222','Guest',null,'1','2017-11-11'),
('16','18911248217',null,'18911248217','Guest',null,'1',null),
('17',null,null,'13911016159','Guest',null,'1','2017-11-12'),
('18',null,null,'18301218698','Guest',null,'1','2017-11-12'),
('19',null,null,'18210495931','Guest',null,'1','2017-11-13'),
('20','尼玛占堆','111111','18689009295','Admin','506284066@qq.com','1','2017-11-14'),
('21','德吉卓嘎','111111','13989902727','Admin','48774306@qq.com','1','2017-11-14'),
('22','旦增加拉','111111','18889096996','Admin','912546159@qq.com','1','2017-11-14'),
('23','132','111111','13211111111','User','11@22.com','1','2017-11-15');
SET FOREIGN_KEY_CHECKS = 1;

