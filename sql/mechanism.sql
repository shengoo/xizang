SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS  `mechanism`;
CREATE TABLE `mechanism` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `abbr` varchar(150) DEFAULT '',
  `leader` varchar(150) DEFAULT '',
  `leadercontact` varchar(150) DEFAULT '',
  `contact` varchar(150) DEFAULT '',
  `contactcontact` varchar(150) DEFAULT '',
  `address` varchar(150) DEFAULT '',
  `supervisor` varchar(150) DEFAULT '',
  `datecreated` date DEFAULT NULL,
  `dateupdated` date DEFAULT NULL,
  `email` VARCHAR(150) NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

insert into `mechanism`(`id`,`abbr`,`leader`,`leadercontact`,`contact`,`contactcontact`,`address`,`supervisor`,`datecreated`,`dateupdated`) values
('2','00','00','00','00','00','000','00','2017-11-08',null),
('3','机构简称机构简称机构简称机构简称','负责人','负责人联系方式','联系人','联系人联系方式','机构地址机构地址机构地址机构地址机构地址','上级机关上级机关','2017-11-08',null),
('8','水利厅','张三','18811199009','张三','18811109990','西藏自治区','中央水利厅','2017-11-09',null),
('16','00','','','','','','','2017-11-12',null),
('17','1','','','','','','','2017-11-12',null),
('18','2','','','','','','','2017-11-12',null),
('19','3','','','','','','','2017-11-12',null),
('20','4','','','','','','','2017-11-12',null),
('21','5','','','','','','','2017-11-12',null),
('22','水土保持','测试','010-1111111','测试','13245678945','北京市海淀区','水利部','2017-11-13',null),
('23','自治区水土保持局','易云飞','13889080915','尼玛占堆','18689009295','','','2017-11-14',null);
SET FOREIGN_KEY_CHECKS = 1;

