## Requirement

### develop 

1. Java 1.8
2. Maven 3.6

### deploy

1. Java 1.8
2. Tomcat

## How to

### install jar to local maven

1. aliyun sdk

```
mvn install:install-file -Dfile=libs/aliyun-java-sdk-core-3.3.1.jar -DgroupId=com.aliyun -DartifactId=aliyun-java-sdk-core -Dversion=3.3.1 -Dpackaging=jar -DgeneratePom=true
```

2. aliyun dysmsapi sdk

```
mvn install:install-file -Dfile=libs/aliyun-java-sdk-dysmsapi-1.0.0.jar -DgroupId=com.aliyun -DartifactId=aliyun-java-sdk-dysmsapi -Dversion=1.0.0 -Dpackaging=jar -DgeneratePom=true
```

### run app

```mvn spring-boot:run```


### create deployable war file 

```mvn package```


### deploy to tomcat

```mvn clean tomcat7:(re)deploy```

